﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ListOfBooks.Models;

namespace ListOfBooks.Data
{
    public class ListOfBooksContext : DbContext
    {
        public ListOfBooksContext (DbContextOptions<ListOfBooksContext> options)
            : base(options)
        {
        }

        public DbSet<ListOfBooks.Models.BookViewModel> BookViewModel { get; set; } = default!;
    }
}
