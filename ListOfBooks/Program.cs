﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ListOfBooks.Data;
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<ListOfBooksContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("ListOfBooksContext") ?? throw new InvalidOperationException("Connection string 'ListOfBooksContext' not found.")));

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Book}/{action=Index}/{id?}");

app.Run();
