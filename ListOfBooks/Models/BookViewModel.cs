﻿using System.ComponentModel.DataAnnotations;

namespace ListOfBooks.Models
{
    public class BookViewModel
    {
        [Key]
        public int BookID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Author { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Harus lebih besar atau sama dengan 1 ")]
        public int Price { get; set; }
    }
}
